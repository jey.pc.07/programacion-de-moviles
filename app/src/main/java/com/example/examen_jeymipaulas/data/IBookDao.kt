package com.example.examen_jeymipaulas.data

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.examen_jeymipaulas.model.BookModel

@Dao
interface IBookDao {

    @Insert(onConflict =  OnConflictStrategy.IGNORE)
    suspend fun addBook(book: BookModel)

    @Update
    suspend fun updateBook(book: BookModel)

    @Delete
    suspend fun deleteBook(book: BookModel)

    @Query( "SELECT * FROM book_table ORDER BY id ASC")
    fun readAllBookData(): LiveData<List<BookModel>>
}