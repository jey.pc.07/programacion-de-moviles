package com.example.examen_jeymipaulas.fragments.delete

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.examen_jeymipaulas.R
import com.example.examen_jeymipaulas.model.BookModel
import com.example.examen_jeymipaulas.viewModel.BookViewModel
import kotlinx.android.synthetic.main.delete_book_fragment.view.*

class DeleteFragment : Fragment() {
    private val args by navArgs<DeleteFragmentArgs>()
    private lateinit var mBookViewModel: BookViewModel

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.delete_book_fragment, container, false)

        mBookViewModel = ViewModelProvider(this).get(BookViewModel::class.java)

        view.txt_book_delete.setText(args.currentBook.title)


        view.btn_approve.setOnClickListener {
            delete()
        }
        view.btn_reject.setOnClickListener {
            goToListBooks()
        }

        return view
    }

    private fun delete() {
        var deleteBook = BookModel(
                args.currentBook.id,
                args.currentBook.title,
                args.currentBook.isbn,
                args.currentBook.author,
                args.currentBook.datePublish,
                args.currentBook.numberPages,
                args.currentBook.description,
                args.currentBook.photoUrl
        )
        mBookViewModel.deleteBook(deleteBook)
        Toast.makeText(requireContext(), "Book Successfully Removed!!", Toast.LENGTH_SHORT).show()
        findNavController().navigate(R.id.action_delete_approved_to_listFragment)

    }

    private fun goToListBooks() {
        findNavController().navigate(R.id.action_delete_reject_to_listFragment)
    }
}