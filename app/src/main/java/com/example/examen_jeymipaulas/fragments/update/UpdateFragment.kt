package com.example.examen_jeymipaulas.fragments.update

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.examen_jeymipaulas.R
import com.example.examen_jeymipaulas.model.BookModel
import com.example.examen_jeymipaulas.viewModel.BookViewModel
import kotlinx.android.synthetic.main.add_book_fragment.*
import kotlinx.android.synthetic.main.update_book_fragment.view.*

class UpdateFragment : Fragment() {

    private val args by navArgs<UpdateFragmentArgs>()
    private lateinit var mBookViewModel : BookViewModel

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        val view =  inflater.inflate(R.layout.update_book_fragment, container, false)

        mBookViewModel = ViewModelProvider(this).get(BookViewModel::class.java)

        view.editTextBookTitle.setText(args.currentBook.title)
        view.editTextBookIsbn.setText(args.currentBook.isbn.toString())
        view.editTextBookAuthor.setText(args.currentBook.author)
        view.editTextBookDatePublish.setText(args.currentBook.datePublish)
        view.editTextBookNumberPages.setText(args.currentBook.numberPages.toString())
        view.editTextBookDescription.setText(args.currentBook.description)
        view.editTextBookPhotoUrl.setText(args.currentBook.photoUrl)

        view.btnUpdateBook.setOnClickListener {
            updateItem()
        }
        return  view
    }

    private  fun updateItem(){
        val title = editTextBookTitle.text.toString()
        val isbn =  editTextBookIsbn.text
        val author = editTextBookAuthor.text.toString()
        val datePublish = editTextBookDatePublish.text.toString()
        val numberPages = editTextBookNumberPages.text
        val description = editTextBookDescription.text.toString()
        val photoUrl = editTextBookPhotoUrl.text.toString()

        if(inputCheck(title, isbn.toString(), author, datePublish, numberPages.toString(),description, photoUrl)) {
            val updateBook = BookModel (
                    args.currentBook.id,
                    title,
                    Integer.parseInt(isbn.toString()),
                    author,
                    datePublish,
                    Integer.parseInt(numberPages.toString()),
                    description,
                    photoUrl)

            mBookViewModel.updateBook(updateBook)
            Toast.makeText(requireContext(), "Book Updated Succesfully!!", Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_updateFragment_to_listFragment)
        }else{
            Toast.makeText(requireContext(), "Please Fill All Fields", Toast.LENGTH_SHORT).show()
        }
    }

    private fun inputCheck(
            title: String,
            isBN: String,
            pages: String,
            author: String,
            datePublish: String,
            description: String,
            imageUrl: String
    ): Boolean {
        return !(title.isEmpty() &&
                isBN.isEmpty() &&
                pages.isEmpty() &&
                datePublish.isEmpty() &&
                author.isEmpty() &&
                description.isEmpty() &&
                imageUrl.isEmpty())
    }
}