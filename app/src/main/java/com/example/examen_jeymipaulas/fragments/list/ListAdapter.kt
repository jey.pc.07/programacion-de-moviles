package com.example.examen_jeymipaulas.fragments.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.examen_jeymipaulas.R
import com.example.examen_jeymipaulas.model.BookModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.book_row.view.*

class ListAdapter : RecyclerView.Adapter<ListAdapter.MyViewHolder>() {

    private var bookList = emptyList<BookModel>()

    class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.book_row, parent, false))
    }

    override fun getItemCount(): Int {
        return  bookList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val picasso = Picasso.get()
        val currentItem =  bookList[position]
        holder.itemView.txt_title.text = currentItem.title
        holder.itemView.txt_author.text = currentItem.author
        holder.itemView.txt_datePublish.text = currentItem.datePublish
        holder.itemView.txt_isbn.text = currentItem.isbn.toString()
        holder.itemView.txt_description.text = currentItem.description
        holder.itemView.txt_pagesNumbers.text = currentItem.numberPages.toString()
        picasso.load(currentItem.photoUrl)
                .into(holder.itemView.image_url)

        holder.itemView.rowLayout.setOnClickListener {
            val action = ListFragmentDirections.actionListFragmentToUpdateFragment(currentItem)
            holder.itemView.findNavController().navigate(action)
        }

        holder.itemView.btnEdit.setOnClickListener {
            val action = ListFragmentDirections.actionListFragmentToUpdateFragment(currentItem)
            holder.itemView.findNavController().navigate(action)
        }

        holder.itemView.btnDelete.setOnClickListener {
            val action = ListFragmentDirections.actionListFragmentToDeleteFragment(currentItem)
            holder.itemView.findNavController().navigate(action)
        }
    }

    fun setData (books : List <BookModel>){
        this.bookList = books
        notifyDataSetChanged()
    }
}