package com.example.examen_jeymipaulas.fragments.add

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.examen_jeymipaulas.R
import com.example.examen_jeymipaulas.model.BookModel
import com.example.examen_jeymipaulas.viewModel.BookViewModel
import kotlinx.android.synthetic.main.add_book_fragment.*
import kotlinx.android.synthetic.main.add_book_fragment.view.*

class AddFragment : Fragment() {

    private lateinit var mBookViewModel: BookViewModel

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val view =  inflater.inflate(R.layout.add_book_fragment,  container, false);

        mBookViewModel = ViewModelProvider(this).get(BookViewModel::class.java)
        view.btnAddBook.setOnClickListener{
            insertDataToDataBase()
        }
        return  view
    }

    private fun insertDataToDataBase() {
        val title = editTextBookTitle.text.toString()
        val isbn = editTextBookIsbn.text.toString()
        val author = editTextBookAuthor.text.toString()
        val datePublish = editTextBookDatePublish.text.toString()
        val numberPages = editTextBookNumberPages.text.toString()
        val description = editTextBookDescription.text.toString()
        val photoUrl = editTextBookPhotoUrl.text.toString()

        if(inputCheck(title, isbn, author, datePublish, numberPages, description, photoUrl)){
            val book = BookModel(
                    0,
                    title,
                    Integer.parseInt(isbn),
                    author,
                    datePublish,
                    Integer.parseInt(numberPages),
                    description,
                    photoUrl
            )
            mBookViewModel.addBook(book)
            Toast.makeText(requireContext(), "Book Successfully Added!!", Toast.LENGTH_LONG).show()
            findNavController().navigate(R.id.action_addFragment_to_listFragment)
        }
        else {
            Toast.makeText(requireContext(), "Please Fill All Fields", Toast.LENGTH_LONG).show()
        }
    }

    private fun inputCheck (
        title: String,
        isbn: String,
        author: String,
        numberPages: String,
        datePublish: String,
        description: String,
        imageUrl: String
    ): Boolean {
        return !(title.isEmpty() &&
                isbn.isEmpty() &&
                numberPages.isEmpty() &&
                datePublish.isEmpty() &&
                author.isEmpty() &&
                description.isEmpty() &&
                imageUrl.isEmpty())
    }
}