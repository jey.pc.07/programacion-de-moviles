package com.example.examen_jeymipaulas.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.examen_jeymipaulas.data.BookDatabase
import com.example.examen_jeymipaulas.model.BookModel
import com.example.examen_jeymipaulas.repository.BookRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class BookViewModel(application: Application): AndroidViewModel(application) {

    val readAllData: LiveData<List<BookModel>>
    private val repository: BookRepository
    init{
        val bookDao= BookDatabase.getDatabase(
                application
        ).bookDao()
        repository = BookRepository(bookDao)
        readAllData = repository.readAllData
    }

    fun addBook (book: BookModel){
        viewModelScope.launch(Dispatchers.IO){
            repository.addBook(book)
        }
    }

    fun updateBook (book: BookModel){
        viewModelScope.launch (Dispatchers.IO){
            repository.updateBook(book)
        }
    }
    fun deleteBook (book: BookModel){
        viewModelScope.launch (Dispatchers.IO){
            repository.deleteBook(book)
        }
    }
}