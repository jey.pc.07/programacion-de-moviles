package com.example.examen_jeymipaulas.repository

import androidx.lifecycle.LiveData
import com.example.examen_jeymipaulas.data.IBookDao
import com.example.examen_jeymipaulas.model.BookModel

class BookRepository (private val bookDao: IBookDao) {

    val readAllData: LiveData<List<BookModel>> = bookDao.readAllBookData()

    suspend fun  addBook  (book: BookModel){
        bookDao.addBook(book)
    }

    suspend fun updateBook(book: BookModel){
        bookDao.updateBook(book)
    }

    suspend fun deleteBook(book: BookModel){
        bookDao.deleteBook(book)
    }
}