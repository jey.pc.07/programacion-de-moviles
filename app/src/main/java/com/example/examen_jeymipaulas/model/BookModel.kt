package com.example.examen_jeymipaulas.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize
/*un libro (titulo, isbn, autor, fecha de publicación, nro de paginas, descripción breve, foto de portada(url)).*/
@Parcelize
@Entity  (tableName="book_table")
data class BookModel (
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val title: String,
    val isbn: Int,
    val author: String,
    val datePublish: String,
    val numberPages: Int,
    val description: String,
    val photoUrl:String
) : Parcelable